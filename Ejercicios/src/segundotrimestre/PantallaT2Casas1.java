package segundotrimestre;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PantallaT2Casas1 {

	private JFrame frame;
	private JTextField textField;
	private JLabel lblNewLabel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaT2Casas1 window = new PantallaT2Casas1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PantallaT2Casas1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(127, 255, 000));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ingresar curso al que pertenece");
		lblNewLabel.setFont(new Font("TimesRoman", Font.PLAIN, 15));
		lblNewLabel.setBounds(29, 42, 226, 19);
		frame.getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(284, 39, 130, 26);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Mi curso es...");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int curso = Integer.parseInt(textField.getText());
				
				if (curso == 0)
					lblNewLabel_1.setText("Es de al jardin de infantes");
				if (curso >= 1)
					if (curso <= 6)
						lblNewLabel_1.setText("Es de al nivel primario");
					else if (curso >= 7)
						if (curso <= 12)
							lblNewLabel_1.setText("Es de al nivel secundario");
						else 
							lblNewLabel_1.setText("Error");
				
				
			}
		});
		btnNewButton.setBounds(155, 106, 117, 29);
		frame.getContentPane().add(btnNewButton);
		
		lblNewLabel_1 = new JLabel("Nivel al que pertenece");
		lblNewLabel_1.setBackground(new Color(255, 000, 193));
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setFont(new Font("TimesRoman", Font.ITALIC, 15));
		lblNewLabel_1.setBounds(98, 175, 239, 26);
		frame.getContentPane().add(lblNewLabel_1);
	}

}
