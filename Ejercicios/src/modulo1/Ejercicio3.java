package modulo1;

public class Ejercicio3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Tecla de Escape |\t Significado");
		System.out.println("----------------|-------------------------------------------------------------------------------");
		System.out.println("\t\t|\t\t");
		System.out.println("\\n \t\t|\t Significa nueva l�nea");
		System.out.println("\\t \t\t|\t Significa un tab de espacio");
		System.out.println("\\\" \t\t|\t Es para poner \"(comillas dobles) dentro del texto por ejemplo");
		System.out.println("\t\t|\t \"Belencita\"");
		System.out.println("\\\\ \t\t|\t Se utiliza para escribir la \\ dentro del texto, por ejemplo \\algo\\ ");
		System.out.println("\\\' \t\t|\t Se utiliza para la \'(comilla simple) para escribir por ejemplo \'Princesita\'");
}

}
